package org.sber;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;



public class Server {

     static List<ServerThread> listUsers = new ArrayList<>();
     static Multimap<String, String> allMessages = ArrayListMultimap.create();

    public static void main(String[] args) throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(8888)) {
            System.out.println("Сервер запущен");
            while (true){
                Socket socket = serverSocket.accept();
                System.out.println("подключен новый клиент");
                listUsers.add(new ServerThread(socket));
            }
        }
    }
}

class ServerThread extends Thread {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private String nickname;

    public ServerThread(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream());
        start();
    }

    @Override
    public void run() {
        try {
            send("Введи свой nickname");
            nickname = in.readLine();
            send("Привет " + nickname );

            while (true) {
                String message = in.readLine();
                if (message.equalsIgnoreCase("allMessages")){  // Получить все сообщения адресованные текущему пользователю
                    send("Все сообщения пользоваетля " + nickname+ "\n");
                    Server.allMessages.entries().stream().filter(entry -> entry.getKey().equals(nickname))
                            .forEach(entry -> send(entry.getValue()));
                    continue;
                }
                Server.allMessages.put(nickname, message);
                System.out.println("Сервер получил сообщение от " + nickname + " : " + message);
                Server.listUsers.stream().forEach(user -> send(nickname + " : " + message));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void send(String message) {
            out.println(message);
            out.flush();
    }
}
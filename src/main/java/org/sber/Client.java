package org.sber;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {
        new ClientThread("127.0.0.1", 8888).start();
    }
}

class ClientThread extends Thread {

    private String address;
    private int port;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    Scanner console;

    public ClientThread(String address, int port) throws IOException {
        try {
            socket = new Socket(address, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
            console = new Scanner(System.in);
            new ReadMessage().start();
            new WriteMessage().start();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            socket.close();
            in.close();
            out.close();
        }
    }

    class ReadMessage extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println(in.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class WriteMessage extends Thread {

        @Override
        public void run() {
            while (true) {
                String message = console.nextLine();
                out.println(message);
                out.flush();
            }
        }
    }
}